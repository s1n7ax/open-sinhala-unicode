import RuleModel from "./rule-model";

export default class KeyBindDetailsModel {
    char: string;
    rules: Array<RuleModel>;

    constructor(char: string, rules: Array<RuleModel>) {
        this.char = char;
        this.rules = rules;
    }
}