import RuleModel from "./models/rule-model";

/**
 * Do the changes before or after the conversion
 * This can be used to auto correction of the words
 */
export default interface ILanguageModifier {
    /**
     * Modifier that will be ran before the language conversion
     *
     * @param letter
     *  letter that should  done the language conversion
     *
     * @param currentWord
     *  data about the current word
     *
     * @param data
     *  any other data required by the modifier
     *
     * @return
     *  modified current word
     */
    beforeCallback(letter: string, currentWord?: string, ...data: Array<any>): string;

    /**
     * Modifier that will be ran after the language conversion
     *
     * @param letter
     *  letter that should  done the language conversion
     *
     * @param currentWord
     *  data about the current word
     *
     * @param conversionData
     *  conversion data that is returned by the converter
     *
     * @param data
     *  any other data required by the modifier
     *
     * @return
     *  modified conversion data
     */
    afterCallback(letter: string, currentWord: string, conversionData: RuleModel, ...data: Array<any>): RuleModel;
}