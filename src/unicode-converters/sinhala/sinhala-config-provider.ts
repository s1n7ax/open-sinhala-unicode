import IConfigProvider from "../../i-config-provider";
import KeyBindDetailsModel from "../../models/key-bind-details-model";
import ConfigModel from "../../models/config-model";

export default class SinhalaConfigProvider implements IConfigProvider {

    getConfig(url: string): Promise<ConfigModel> {

        return new Promise<ConfigModel>((resolve, reject) => {
            fetch(url)
                .then(async response => {

                    // fetch the configuration
                    if(!response.ok)
                        throw new Error("Unable to get data " + response.status);

                    // json to map
                    let json = await response.json();

                    let configVariables: Map<string, Array<string>>
                        = SinhalaConfigProvider.jsonToMap<Array<string>>(json["variables"]);

                    let configMapping: Map<string, KeyBindDetailsModel>
                        = SinhalaConfigProvider.jsonToMap<KeyBindDetailsModel>(json["mapping"]);

                    let config = new ConfigModel(configVariables, configMapping);

                    // return config
                    resolve(config);
                })
                .catch(reject);
        });
    }

    private static jsonToMap<T> (json: any): Map<string, T> {
        let map = new Map<string, T>();

        for(let key in json) {
            map.set(key , json[key]);
        }

        return map
    }
}
